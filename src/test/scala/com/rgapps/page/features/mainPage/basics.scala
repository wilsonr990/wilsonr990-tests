package com.rgapps.page.features.mainPage

import com.rgapps.page.contexts.RawDataContext._
import com.rgapps.page.contexts.UIContext._
import com.rgapps.page.contexts.UrlsContext._
import cucumber.api.scala.{EN, ScalaDsl}
import cucumber.utils.ComposedSteps
import org.scalatest.Matchers
import org.scalatest.concurrent.Eventually

class basics extends ScalaDsl with EN with Matchers with Eventually {

  // GIVEN
  Given("""^[an user ]*?is in my main page$""") { () =>
    ComposedSteps.RunStep("goes to my main page")
    ComposedSteps.RunStep("should be loaded correctly")
  }

  Given("""^[an user ]*?is in my main page with languages button visible$""") { () =>
    ComposedSteps.RunStep("is in my main page")
    ComposedSteps.RunStep("should contain languages Button eventually (3000 ms)")
    // TODO: this step should not be necessary!
    ComposedSteps.RunStep("should not contain buttons with the languages I speak")
  }

  Given("""^[an user ]*?has the mouse over the languages Button$""") { () =>
    ComposedSteps.RunStep("is in my main page with languages button visible")
    ComposedSteps.RunStep("put the mouse over the languages Button")
  }

  // WHEN
  When("""^[an user ]*?goes to my main page$""") {
    page.navigateTo(pageDomainUrl)
  }

  When("""^[an user ]*?put the mouse over the languages Button$""") {
    mainPage.hoverOn languageButton()
  }

  When("""^[an user ]*?put the mouse somewhere else$""") {
    mainPage.unHoverOn languageButton()
  }

  // THEN
  Then("""^[the page ]*?should be loaded correctly$""") { () =>
    page.title should be(pageTitle)
  }

  Then("""^[the page ]*?should contain my name$""") { () =>
    mainPage.nameLabel.get.text should be(myName)
  }

  Then("""^[the page ]*?should contain my logo$""") { () =>
    mainPage.logoTextLabel.get.text should be(myLogo)
  }

  Then("""^[the page ]*?should contain languages Button$""") { () =>
    mainPage.languageButton.get.text should be(languagesButtonText)
  }

  Then("""^[the page ]*?should not contain languages Button$""") { () =>
    mainPage.languageButton should be(None)
  }

  Then(
    """^[the page ]*?should not contain buttons with the languages I speak$"""
  ) { () =>
    mainPage.languageButtons should have size 0
  }

  Then("""^[the page ]*?should contain buttons with the languages I speak$""") {
    () =>
      mainPage.languageButtons.map(_.get.text) should contain allElementsOf theLanguagesISpeak
  }
}
