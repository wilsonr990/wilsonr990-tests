package com.rgapps.page.features

import com.rgapps.page.contexts.UIContext
import cucumber.api.scala.{EN, ScalaDsl}
import cucumber.utils.ComposedSteps
import org.scalatest.Matchers
import org.scalatest.concurrent.{Eventually, IntegrationPatience}
import org.scalatest.time.{Milliseconds, Span}

class utilitySteps extends ScalaDsl with EN with Matchers with Eventually with IntegrationPatience {

  Before() { _ =>
    UIContext.switchToDefaultBrowser()
  }

  // Allow steps to be executed a number of times
  When("""^(.*) \(([0-9]*) times\)$""") { (step: String, times: String) =>
    0.to(times.toInt).foreach(_ => {
      ComposedSteps.RunStep(step)
    })
  }

  // Add eventually
  When("""^(.*) eventually \(([0-9]*) ms\)$""") { (step: String, ms: String) =>
    try {
      eventually(timeout(Span(ms.toInt, Milliseconds)), interval(Span(ms.toInt / 10, Milliseconds))) {
        ComposedSteps.RunStep(step)
      }
    } catch {
      case e: Throwable => {
        e.setStackTrace(e.getStackTrace.filter(e => e.toString.contains("com.rga.")))
        throw e
      }
    }
  }

}
