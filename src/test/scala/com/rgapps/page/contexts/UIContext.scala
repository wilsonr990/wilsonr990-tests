package com.rgapps.page.contexts

import com.rgapps.page.contexts.BrowserContext.browserProperties
import com.rgapps.page.ui.MainPage
import com.rgapps.page.utils.browser.{Page, WebDriverFactory}
import org.openqa.selenium.WebDriver

object UIContext {
  private var driver: Option[WebDriver] = None
  private var secondDriver: Option[WebDriver] = None

  def page: Page = {
    new Page(getDriver)
  }

  def mainPage: MainPage = {
    MainPage(getDriver)
  }

  private def getDriver: WebDriver = {
    secondDriver.getOrElse {
      driver.getOrElse {
        driver = Some(WebDriverFactory.generate(browserProperties))
        driver.get
      }
    }
  }

  def switchToSecondBrowser() {
    secondDriver = Some(WebDriverFactory.generate(browserProperties))
  }

  def switchToDefaultBrowser() {
    secondDriver.foreach(_.quit())
    secondDriver = None
  }

  def close() {
    driver.foreach(_.quit())
    driver = None

    secondDriver.foreach(_.quit())
    secondDriver = None
  }
}
