package com.rgapps.page.contexts

object RawDataContext {
  val pageTitle: String = "wilsonr990"

  // TODO 1: this information should be configured in the application and saved in a DB
  val myName: String = "Wilson René Guevara Arévalo"
  val myLogo: String = "Wilsonr990"

  val languagesButtonText: String = "Languages I Speak"
  val theLanguagesISpeak: List[String] = List("ESPAÑOL", "ENGLISH", "العربي")
}
