@Smoke
Feature: Smoke tests
  A user can iterate with the page in the following form

  Background:

  Scenario: Smoke
#  Scenario: Content Regression
    When an user goes to my main page
    Then the page should be loaded correctly
    Then the page should contain my name
    And should contain my logo
    And should not contain languages Button
    And should not contain buttons with the languages I speak
    Then should contain languages Button eventually (3000 ms)
    And should not contain buttons with the languages I speak
    When put the mouse over the languages Button
    Then the page should contain buttons with the languages I speak eventually (3000 ms)
    When put the mouse somewhere else
    And should not contain buttons with the languages I speak eventually (3000 ms)