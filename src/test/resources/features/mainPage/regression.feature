@Regression @Content
Feature: Content

  Background:

  Scenario: Content Regression
#  Scenario: User can enter to the page
    When an user goes to my main page
    Then the page should be loaded correctly

#  Scenario: Page contents my profile information
#    Given an user is in my main page
    Then the page should contain my name
    And should contain my logo
    And should not contain languages Button
    And should not contain buttons with the languages I speak

#  Scenario: Page eventually show language button
#    Given an user is in my main page
    Then should contain languages Button eventually (3000 ms)
    And should not contain buttons with the languages I speak

#  Scenario: Languages I speak appear when language button is hover
#    Given an user is in my main page with languages button visible
    When put the mouse over the languages Button
    Then the page should contain buttons with the languages I speak eventually (4000 ms)

#  Scenario: Languages I speak disappear when language button is unhover
#    Given an user has the mouse over the languages Button
    When put the mouse somewhere else
    And should not contain buttons with the languages I speak eventually (4000 ms)