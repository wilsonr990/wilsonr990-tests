package com.rgapps.page.utils

import scala.concurrent.duration.{Duration, MINUTES, SECONDS}

object DateUtils {

  def parseDuration(startTime: Long, stopTime: Long): String = {
    val duration = Duration.fromNanos(stopTime - startTime)

    val minutes = duration.toMinutes
    val seconds = duration.minus(duration.toMinutes, MINUTES).toSeconds
    val millis = duration.minus(duration.toSeconds, SECONDS).toMillis
    if (duration.toMinutes > 0) f"$minutes m $seconds s $millis ms"
    else if (duration.toSeconds > 0) f"$seconds s $millis ms"
    else f"$millis ms"
  }
}
