package com.rgapps.page.utils.browser

import java.net.URL
import java.util.concurrent.TimeUnit

import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.{ChromeDriver, ChromeOptions}
import org.openqa.selenium.firefox.{FirefoxDriver, FirefoxOptions}
import org.openqa.selenium.remote.RemoteWebDriver

object WebDriverFactory {
  def generate(properties: BrowserProperties): WebDriver = {
    val driver = properties.name match {
      case "chrome" =>
        System.setProperty("webdriver.chrome.driver", properties.driverPath)
        val chromeOptions = new ChromeOptions
        if (properties.headless) chromeOptions.addArguments("--headless")
        chromeOptions.addArguments("--ignore-certificate-errors")
        new ChromeDriver(chromeOptions)

      case "firefox" =>
        System.setProperty("webdriver.gecko.driver", properties.driverPath)
        val firefoxOptions = new FirefoxOptions()
        if (properties.headless) firefoxOptions.setHeadless(true)
        firefoxOptions.setAcceptInsecureCerts(true)
        new FirefoxDriver(firefoxOptions)

      case "firefox_remote" =>
        val firefoxOptions: FirefoxOptions = new FirefoxOptions()
        firefoxOptions.setAcceptInsecureCerts(true)
        firefoxOptions.setProxy(null)
        if (properties.headless) firefoxOptions.setHeadless(true)

        new RemoteWebDriver(new URL(properties.driverPath), firefoxOptions);

      case "chrome_remote" =>
        val chromeOptions = new ChromeOptions()
        chromeOptions.addArguments("--ignore-certificate-errors")
        if (properties.headless) chromeOptions.addArguments("--headless")

        new RemoteWebDriver(new URL(properties.driverPath), chromeOptions);
    }
    driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS)
    driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS)
    driver.manage().timeouts().setScriptTimeout(5, TimeUnit.SECONDS)
    driver.manage().window().maximize()
    driver
  }
}