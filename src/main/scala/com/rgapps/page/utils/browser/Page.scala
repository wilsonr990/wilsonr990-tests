package com.rgapps.page.utils.browser

import org.openqa.selenium._
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.support.ui.ExpectedConditions._
import org.openqa.selenium.support.ui.WebDriverWait
import org.scalatest.Matchers
import org.scalatest.concurrent.Eventually
import org.scalatest.selenium.{Driver, WebBrowser}
import org.scalatest.time.{Seconds, Span}

class Page(driver: WebDriver) extends WebBrowser with Driver with Matchers with Eventually {
  override implicit val webDriver: WebDriver = driver
  val actions = new Actions(webDriver)
  val jse2: JavascriptExecutor = webDriver.asInstanceOf[JavascriptExecutor]
  val webDriverWait: WebDriverWait = new WebDriverWait(webDriver, 1, 100)

  def currentUrl: String = webDriver.getCurrentUrl

  def navigateTo(url: String) {
    println("\t\tNavigating to " + url)

    webDriver get url
    eventually(timeout(Span(5, Seconds)), interval(Span(1, Seconds))) {
      title should not be ""
    }
    println("\t\tNavigated directly to " + url)
  }

  def title: String = webDriver.getTitle

  def screenshot: Array[Byte] = {
    driver.asInstanceOf[TakesScreenshot].getScreenshotAs(OutputType.BYTES)
  }

  object get {
    def element(query: Query): WebElement = {
      eventually {
        webDriverWait until presenceOfElementLocated(query.by)
        query.findAllElements.toList.head.underlying
      }
    }

    def elementOrNone(query: Query): Option[WebElement] = {
      try Some(get element query) catch {
        case _: Exception => None
      }
    }

    def elements(query: Query): List[WebElement] = {
      try {
        webDriverWait until presenceOfAllElementsLocatedBy(query.by)
        query.findAllElements.map(e => e.underlying).toList
      } catch {
        case _: WebDriverException => List()
      }
    }
  }

  object send {

    case class keys(keys: String) {

      object to {
        // should be used in elements that are inputs, textAreas, etc.
        def formElement(query: Query) {
          val element = get element query
          if (element.getAttribute("value") != keys) {
            eventually(timeout(Span(5, Seconds)), interval(Span(1, Seconds))) {
              element.clear()
              element.sendKeys(keys)
              element.getAttribute("value") should be(keys)
            }
          }
        }

        // should be used in elements that contains the text but are not editable
        def deferredElement(query: Query) {
          val element = get element query
          if (element.getText.replaceAll("\\s", "") != keys.replaceAll("\\s", "")) {
            eventually(timeout(Span(5, Seconds)), interval(Span(1, Seconds))) {
              sendClick to query
              actions
                .keyDown(Keys.CONTROL)
                .sendKeys("a")
                .keyUp(Keys.CONTROL)
                .sendKeys(Keys.DELETE)
                .perform()
              driver.switchTo() activeElement() sendKeys keys
              actions
                .sendKeys(Keys.DELETE)
                .sendKeys(Keys.DELETE)
                .sendKeys(Keys.DELETE)
                .sendKeys(Keys.DELETE)
                .perform()
              element.getText.replaceAll("\\s", "") should be(
                keys.replaceAll("\\s", ""))
            }
          }
        }
      }

    }

  }

  // Put element centered in  the screen (if possible)
  // prevent corners to affect any kind of test while clicking
  private def centerScreenAround(query: Query) = {
    val location = query.findAllElements.toList.head.underlying.getLocation
    jse2.executeScript("window.scrollTo(arguments[0],arguments[1])",
      location.x.toString,
      (location.y - (webDriver
        .manage() window()).getSize.height / 2).toString)
  }

  object move {
    def to(query: Query) {
      eventually {
        centerScreenAround(query)
        actions
          .moveToElement(query.findAllElements.toList.head.underlying)
          .perform()
        webDriverWait until visibilityOfElementLocated(query.by)
        webDriverWait until elementToBeClickable(
          query.findAllElements.toList.head.underlying)
      }
    }

    def outOf(query: Query) {
      eventually {
        centerScreenAround(query)
        val size = query.findAllElements.toList.head.underlying.getSize
        actions
          .moveByOffset(size.width, size.height)
          .perform()
      }
    }
  }

  object sendClick {
    def to(query: Query) {
      eventually {
        move to query
        query.findAllElements.toList.head.underlying click()
      }
    }
  }

}