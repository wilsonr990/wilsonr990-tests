package com.rgapps.page.utils

import java.io.File

object FileUtils {

  def getFileTree(f: File): Stream[File] =
    f #:: (if (f.isDirectory) f.listFiles().toStream.flatMap(getFileTree)
    else Stream.empty)

  def getFilesWithExtensionInPath(path: String, ext: String): List[String] = {
    val folder = new File(path)
    getFileTree(folder).collect {
      case file if file.getName.endsWith(ext) => file.getPath
    }.toList
  }

  def getFilesWithExtensionInResourcesPath(path: String, ext: String): List[String] = {
    getFilesWithExtensionInPath(parseFilePathFromResources(path), ext)
  }

  def parseFilePathFromResources(path: String): String = {
    val fullPath =
      if (path.startsWith("/"))
        getClass.getResource(path).getPath
      else
        getClass.getResource("/" + path).getPath
    if (fullPath.startsWith("/C:"))
      fullPath.substring(1).replace("/", "\\").replace("%20", " ")
    else
      fullPath.replace("%20", " ")
  }
}
