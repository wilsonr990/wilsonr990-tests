#!/bin/bash
set -eux


tags=${1:-'not @None'}
PAGE_DOMAIN=${QA_PUBLIC_IP:-'172.17.0.1'}

# Change to Path
initialPath=$(pwd)
thisFileThis="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

cd "${thisFileThis}"/../

seleniumIp="$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' selenium)"
#seleniumIp="172.17.0.1"

# QA_PUBLIC_IP is configured when deploying according to infrastructure
export BROWSER_NAME='chrome_remote'
export BROWSER_WEB_DRIVER_PATH="http://$seleniumIp:4444/wd/hub"
export BROWSER_IS_HEADLESS='true'
export DOMAIN=${PAGE_DOMAIN}
sbt "cucumber --tags \"not @Manual and $tags\""

# Go back to initial Path
cd ${initialPath}

